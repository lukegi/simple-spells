package simplespells;

import net.minecraft.block.Block;
import net.minecraft.block.material.Material;
import net.minecraft.client.renderer.block.model.ModelResourceLocation;
import net.minecraft.item.Item;
import net.minecraft.util.ResourceLocation;
import net.minecraftforge.client.event.ModelRegistryEvent;
import net.minecraftforge.client.model.ModelLoader;
import net.minecraftforge.event.RegistryEvent;
import net.minecraftforge.fml.common.eventhandler.SubscribeEvent;
import net.minecraftforge.fml.common.registry.ForgeRegistries;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;
import simplespells.block.ModBlock;
import simplespells.item.ModItem;
import simplespells.reference.Refs;

import static simplespells.block.ModBlocks.*;
import static simplespells.item.ModItems.*;

public class ModRegistry {

    private static final ResourceLocation RESOURCE_LOCATION_ITEMS = Refs.getRL("items");

    private static void registerBlocks(RegistryEvent.Register<Block> event) {
        registerBlock(TEST_BLOCK = new ModBlock(Material.AIR), Refs.TEST_BLOCK);
    }

    private static void registerTileEntities() {
        // NOOP
    }

    @SideOnly(Side.CLIENT)
    private static void registerItemModels() {
        // ItemBlocks
        registerInventoryModel(TEST_BLOCK.getItemBlock());
        // Items
        registerModItemModel(TEST_ITEM, Refs.TEST_ITEM);
    }

    @SideOnly(Side.CLIENT)
    private static void registerStateMappers() {
        // NOOP
    }

    private static void registerItems(RegistryEvent.Register<Item> event) {
        // ItemBlocks
        registerItem(TEST_BLOCK.getItemBlock(), Refs.TEST_BLOCK);
        // Items
        registerItem(TEST_ITEM = new ModItem(), Refs.TEST_ITEM);
    }

    private static void registerOreDict() {
        // NOOP
    }

    public static class EventHandler {
        @SubscribeEvent
        public static void onRegisterBlocks(RegistryEvent.Register<Block> event) {
            registerBlocks(event);
            registerTileEntities();
        }

        @SubscribeEvent
        public static void onRegisterItems(RegistryEvent.Register<Item> event) {
            registerItems(event);
            registerOreDict();
        }
    }

    @SideOnly(Side.CLIENT)
    public static class EventHandlerClient {
        @SubscribeEvent
        public static void onModelRegister(ModelRegistryEvent event) {
            registerItemModels();
            registerStateMappers();
        }
    }

    private static void registerBlock(Block block, String name) {
        block.setUnlocalizedName(name);
        ForgeRegistries.BLOCKS.register(block);
    }

    private static void registerItem(Item item, String name) {
        item.setUnlocalizedName(name);
        ForgeRegistries.ITEMS.register(item);
    }

    @SideOnly(Side.CLIENT)
    private static void registerInventoryModel(Item item) {
        registerInventoryModel(item, 0);
    }

    @SideOnly(Side.CLIENT)
    private static void registerInventoryModel(Item item, int meta) {
        ModelResourceLocation mrl = new ModelResourceLocation(item.getRegistryName(), "inventory");
        registerInventoryModel(item, meta, mrl);
    }

    @SideOnly(Side.CLIENT)
    private static void registerInventoryModel(Item item, int meta, ModelResourceLocation mrl) {
        ModelLoader.setCustomModelResourceLocation(item, meta, mrl);
    }

    @SideOnly(Side.CLIENT)
    private static void registerModItemModel(ModItem item, String name) {
        ModelLoader.setCustomModelResourceLocation(item, 0, new ModelResourceLocation(RESOURCE_LOCATION_ITEMS, "item=" + name));
    }
}
