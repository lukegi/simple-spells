package simplespells.reference;

import net.minecraft.util.ResourceLocation;
import net.minecraftforge.registries.IForgeRegistryEntry;

import java.util.HashMap;

public class Refs {
    // MOD INFO
    public static final String MOD_ID = "simplespells";
    public static final String MOD_NAME = "Simple Spells";
    public static final String VERSION = "0.1.0-alpha";
    public static final String CLIENT_PROXY_CLASS = MOD_ID + ".proxy.ProxyClient";
    public static final String SERVER_PROXY_CLASS = MOD_ID + ".proxy.ProxyServer";
    public static final String GUI_FACTORY_CLASS = MOD_ID + ".gui.GuiFactory";

    // Config Categories
    public static final String CATEGORY_CORE = "core";

    // Block Names
    public static final String TEST_BLOCK = "test_block";

    // Item Names
    public static final String TEST_ITEM = "test_item";

    // I18n Constants
    public static final String LANG_CONFIG_CORE = MOD_ID + ".config.core";

    private static final HashMap<String, ResourceLocation> nameMap = new HashMap<>();

    public static ResourceLocation getRL(String name) {
        ResourceLocation rl = nameMap.get(name);
        if (rl == null) {
            return nameMap.put(name, new ResourceLocation(MOD_ID, name));
        }
        return rl;
    }

    public static <T extends IForgeRegistryEntry<T>> void setRL(IForgeRegistryEntry.Impl<T> registryEntry, String name) {
        registryEntry.setRegistryName(getRL(name));
    }
}
