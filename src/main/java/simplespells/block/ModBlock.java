package simplespells.block;

import net.minecraft.block.Block;
import net.minecraft.block.material.Material;
import net.minecraft.item.Item;
import simplespells.item.block.ModItemBlock;
import simplespells.reference.Refs;

public class ModBlock extends Block {

    public ModBlock(Material materialIn) {
        super(materialIn);
    }

    @Override
    public Block setUnlocalizedName(String name) {
        Refs.setRL(this, name);
        return super.setUnlocalizedName(Refs.MOD_ID + ":" + name);
    }

    public ModItemBlock getItemBlock() {
        Item item = Item.getItemFromBlock(this);
        if (item instanceof ModItemBlock) {
            return (ModItemBlock) item;
        }
        return new ModItemBlock(this);
    }
}
