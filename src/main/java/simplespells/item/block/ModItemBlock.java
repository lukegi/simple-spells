package simplespells.item.block;

import net.minecraft.item.Item;
import net.minecraft.item.ItemBlock;
import simplespells.block.ModBlock;
import simplespells.reference.Refs;

public class ModItemBlock extends ItemBlock {
    private ModBlock block;

    public ModItemBlock(ModBlock block) {
        super(block);
        this.block = block;
    }

    @Override
    public Item setUnlocalizedName(String name) {
        Refs.setRL(this, name);
        return super.setUnlocalizedName(Refs.MOD_ID + ":" + name);
    }

    @Override
    public ModBlock getBlock() {
        return block;
    }
}
