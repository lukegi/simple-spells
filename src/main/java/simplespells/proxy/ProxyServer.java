package simplespells.proxy;

import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;

@SideOnly(Side.SERVER)
public class ProxyServer implements IProxy {
    @Override
    public void preInit() {
        // NOOP
    }
}
