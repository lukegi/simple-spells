package simplespells.gui;

import net.minecraft.client.Minecraft;
import net.minecraft.client.gui.GuiScreen;
import net.minecraft.client.resources.I18n;
import net.minecraftforge.common.config.ConfigElement;
import net.minecraftforge.common.config.Configuration;
import net.minecraftforge.fml.client.IModGuiFactory;
import net.minecraftforge.fml.client.config.DummyConfigElement.DummyCategoryElement;
import net.minecraftforge.fml.client.config.GuiConfig;
import net.minecraftforge.fml.client.config.GuiConfigEntries;
import net.minecraftforge.fml.client.config.IConfigElement;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;
import simplespells.ModConfig;
import simplespells.reference.Refs;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

@SideOnly(Side.CLIENT)
public class GuiFactory implements IModGuiFactory {
    @Override
    public void initialize(Minecraft minecraftInstance) {

    }

    @Override
    public boolean hasConfigGui() {
        return true;
    }

    @Override
    public GuiScreen createConfigGui(GuiScreen parentScreen) {
        return new SSGuiConfig(parentScreen);
    }

    @Override
    public Set<RuntimeOptionCategoryElement> runtimeGuiCategories() {
        return null;
    }

    public static class SSGuiConfig extends GuiConfig {
        public SSGuiConfig(GuiScreen parentScreen) {
            super(parentScreen,
                    getConfigElements(),
                    "simplespells",
                    false,
                    false,
                    I18n.format(Refs.LANG_CONFIG_CORE + ".title"));
        }

        private static List<IConfigElement> getConfigElements() {
            List<IConfigElement> elements = new ArrayList<>();
            elements.add(new DummyCategoryElement("core", Refs.LANG_CONFIG_CORE, CategoryEntryCore.class));
            return elements;
        }

        public static class CategoryEntryCore extends GuiConfigEntries.CategoryEntry {
            public CategoryEntryCore(GuiConfig owningScreen, GuiConfigEntries owningEntryList, IConfigElement configElement) {
                super(owningScreen, owningEntryList, configElement);
            }

            @Override
            protected GuiScreen buildChildScreen() {
                Configuration configuration = ModConfig.getConfig();
                ConfigElement cat_general = new ConfigElement(configuration.getCategory(Refs.CATEGORY_CORE));
                List<IConfigElement> propertiesOnThisScreen = cat_general.getChildElements();
                String windowTitle = configuration.toString();

                return new GuiConfig(this.owningScreen,
                        propertiesOnThisScreen,
                        this.owningScreen.modID,
                        Refs.CATEGORY_CORE,
                        this.configElement.requiresWorldRestart() || this.owningScreen.allRequireWorldRestart,
                        this.configElement.requiresMcRestart() || this.owningScreen.allRequireMcRestart,
                        windowTitle);
            }
        }
    }
}
