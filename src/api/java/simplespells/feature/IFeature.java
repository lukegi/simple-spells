package simplespells.feature;

import net.minecraft.creativetab.CreativeTabs;
import net.minecraftforge.fml.common.Loader;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;

import java.io.File;

public interface IFeature {
    String getName();

    boolean hasBlocks();

    void registerBlocks();

    @SideOnly(Side.CLIENT)
    void registerBlockModels();

    boolean hasItems();

    void registerItems();

    @SideOnly(Side.CLIENT)
    void registerItemModels();

    boolean hasConfig();

    void loadConfigs();

    void syncConfigs();

    boolean hasEventHandler();

    void registerEventHandler();

    boolean hasEntities();

    void registerEntity();

    @SideOnly(Side.CLIENT)
    void registerEntityModels();

    @SideOnly(Side.SERVER)
    boolean hasCommands();

    @SideOnly(Side.SERVER)
    void registerCommands();

    boolean hasGuis();

    void registerGuis();

    boolean hasAdvancements();

    void registerAdvancements();

    boolean hasPackets();

    void registerPackets();

    boolean hasCreativeTab();

    CreativeTabs getCreativeTab();

}
